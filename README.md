#  Looking into the past: exploring time dependencies beyond exponential decay

This project explores models that can measure recurring patterns from datasets and learn the temporal context.
